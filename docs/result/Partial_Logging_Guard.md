## ZooKeeper
ZooKeeper的这部分数据在`Zookeeper_Data_Control_SideEffect_Precision_Manual_Analysis.txt`中
搜索“发现更多的Overhead”
一共6个，有2个在最新版本中已经修复了


## Hadoop
Hadoop的这部分数据在`Hadoop_Data_Control_SideEffect_Precision_Manual_Analysis.txt`中
搜索“检查已有Guard漏掉相关语句的情况”
一共38个，其中有7个是因为副作用计算有误造成的误报
