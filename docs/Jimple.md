## Jimple结构
所有的变量都是JimpleLocal，只能获取到类型和名字，但是很多名字都是$Stack开头的，看不到原始名字。
虽然，Soot提供了use-original-names选项，可以保留原始名字，但是这个特性需要class文件中保留了变量名。
然而有些时候class文件中也没有保留变量名。

只要对象实例的某个成员变量被引用到，就会有一个对应的JimpleLocal。
这个JimpleLocal是在成员变量第一次被引用时，通过一个JAssignStmt来定义的，
leftBox.value是JimpleLocak，rightBox.value是JInstanceFieldRef。
而且，好像只能在这个时候才能获取到对应关系。JimpleLocal本身不保存他们之间的对应关系。

调用语句都实现了InvokeExpr接口，包含7个子接口
```text
DynamicInvokeExpr
InstanceInvokeExpr
InterfaceInvokeExpr
NewInvokeExpr
SpecialInvokeExpr
StaticInvokeExpr
VirtualInvokeExpr
```
其中Jimple里的具体类型包括5种
```text
JDynamicInvokeExpr: 成员方法调用
JInterfaceInvokeExpr: 接口调用
JSpecialInvokeExpr: 
JStaticInvokeExpr： 静态方法调用
JVirtualInvokeExpr：
```
InvokeExpr是Expression，在Jimple中还需要包含在其他Statement中。
如果是单独的方法调用，那么就是包含在JInvokeStmt中；
如果是方法调用的结果用于变量赋值，那么就是包含在JAssignStmt中；
如果是嵌套方法调用，中间的每个方法调用会拆出来包含在JAssignStmt中


