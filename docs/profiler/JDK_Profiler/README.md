# Introduction
We use [DaCapo Benchmark Suite](http://dacapobench.org/) as the target applications to collect execution time of methods in JDK. 
We have tested many well-known profilers and choose VisualVM as our profiler.

# Steps

## DaCapo Benchmark Suite
DaCapo Benchmark Suite (9.12-MR1-bach)
You can see all available options for DaCapo by running `java -jar dacapo-9.12-MR1-bach.jar`  

You can see all available benchmarks by running `java -jar dacapo-9.12-MR1-bach.jar -l`. 
Currently, available benchmarks are
- avrora 
- batik 
- eclipse 
- fop 
- h2 
- jython 
- luindex 
- lusearch 
- lusearch-fix 
- pmd 
- sunflow 
- tomcat 
- tradebeans 
- tradesoap 
- xalan

You can run specific benchmarks by running `java -jar dacapo-9.12-MR1-bach.jar -n [IterationTimes] [BenchmarkName]`


## VisualVM
Before running VisualVM (2.0.4), make sure
- you run it with Oracle JDK 8+.
- the JVM you run VisualVM is the same with target Java application
- change Xmx in ./etc/visualvm.conf

Download latest [VisualVM](https://github.com/oracle/visualvm/releases)

Untar and run visualvm, `./visualvm --jdkhome "<path to JDK>"`

Select and open target application on "Local" applications

Open "Profiler" tab and click "CPU" button

Set CPU settings as following picture shows:

[CPU Settings](VisualVM_Dacapo_Config.png)


Following are [tutorials](https://blogs.oracle.com/nbprofiler/profiling-with-visualvm,-part-2) for VisualVM CPU Profiler

When the CPU button in Profiler tab is pressed, the profiler attaches to the application and starts profiling its performance. At the beginning the profiler needs to instrument some methods of the application, the actual number of instrumented methods is displayed in the Status area. Since the profiler uses dynamic instrumentation, the number of instrumented methods may change during the profiling session.

For performance profiling there are two important settings available: profiling roots and instrumentation filter. In general, profiling roots define when the profiler should start collecting the data whereas instrumentation filter defines packages/classes from which the data should/shouldn't be collected.

**Start profiling** from classes textbox defines the profiling roots. If all the methods were instrumented, the profiling overhead would be enormous and profiling would eventually become unusable. That's why the NetBeans profiler introduces a notion of Root Methods - a way to profile just the code of interest while the rest of the application runs at full speed.

A typical usecase is for example profiling performance of an action invoked by a button click - if the appropriate `actionPerformed()` method is set as a root method, the profiler starts instrumenting and profiling the application once the `actionPerformed()` method is invoked and only profiles the methods called from the `actionPerformed()`. This way the profiling overhead is very small and only relevant profiling results are collected which greatly improves their readability.

Whereas in NetBeans profiler selecting a root method is very natural because of its tight integration with the editor, in VisualVM this might be a bit tricky and not exactly user friendly. That's why VisualVM defines profiling roots by classes - all methods defined in these classes and their innerclasses become root methods. In the similar way, package names can be used as profiling roots. That's how the default profiling roots are defined: `<main_class_package>.**` which means that all methods from `<main_class_package>` and subpackages are profiled.

The default settings work in most cases, but for large applications like application servers it might be useful to tweak the profiling roots to point to a concrete code of interest. For example, when profiling a web application it's much better to set the servlet's package or classname as a profiling root instead of profiling the whole server.

The format for entering the profiling roots in VisualVM is following:

- `org.mypackage.**` defines all methods in `org.mypackage` and all subpackages as root methods
- `org.mypackage.*` defines all methods in `org.mypackage` as root methods
- `org.mypackage.MyClass` defines all methods in `org.mypackage.MyClass` and its innerclasses as root methods
- empty textbox means that all methods executed in the application will be profiled
- Multiple profiling roots (separated by a space and/or comma and/or newline) can be defined for a single profiling session.

Note that it doesn't make sense to define `org.mypackage.MyClass.*` or `org.mypackage.MyClass.**` as profiling roots since the tool always selects all methods of the class and its innerclasses as profiling roots.

**Profile only classes** / Do not profile classes textbox defines the instrumentation filter. This is exactly the same option as Quick Filter in the NetBeans profiler. It either defines the only packages/classes to be profiled or packages/classes not to be profiled. For example, if only an application's code should be profiled without any outgoing calls to other frameworks or JDK code, this option should be set to Profile only classes, `<application_package>.*`. If all the code except JDK classes from `java` and `javax` packages should be profiled, this option should be set to Do not profile classes, `java.*, javax.*`. The default instrumentation filter is Do not profile classes, `java.*, javax.*, sun.*, sunw.*, com.sun.*` + `com.apple.*, apple.awt.*` on Mac OS X which filters out all the JDK code.

The format for entering instrumentation filter is following:

- `org.mypackage.*` selects all classes from `org.mypackage` and subpackages to be profiled or to be excluded
- `org.mypackage`. selects all classes from `org.mypackage` to be profiled or to be excluded
- empty textbox selects all classes of the application to be profiled
- Multiple filter values (separated by a space and/or comma and/or newline) can be defined for a single profiling session

The most important action is **Take Snapshot of Collected Results** (available also as Profiler Snapshot in context menu of profiled application in Applications tree). It takes a snapshot of collected profiling results (compatible with NetBeans profiler snapshot) and opens it in a separate tab. This snapshot provides several different views: Call Tree displaying methods call tree starting by threads, Hot Spots displaying a list of all profiled methods sorted by their execution time and Combined view showing both call tree and list of profiled methods. The last view Info displays basic snapshot information and detailed profiling configuration of the snapshot. There's one handy action available in context menu of each method, it's called Show Back Traces and displays all the places from where the method is being called.

[CPU Profiling Result](CPUProfiler.gif)

Following are explanation of metrics returned by CPU Profiler, copied from this [tutorial](https://engineering.talkdesk.com/ninjas-guide-to-getting-started-with-visualvm-f8bff061f7e7)

- **Self time**: How much time the profiler observed the listed method spent executing the method’s own code (e.g. if the method calls another method, it will not include that time). Note that packages configured as “not profiled” (as seen in the first picture of the section) are counted as “a method’s own code”. In practice, this means that if, for instance, you are doing a lot of string manipulation, time spent in JDK string code will be counted as self time (because JDK classes are configured as “not profiled” by default).
- **Self time (CPU)**: How much CPU time was actually spent by the method. For instance in the image above, we can see a mostly-idle system that apparently spends a lot of time executing a small number of the methods. In reality, little CPU time is actually spent, because the code is just calling Thread.sleep().
- **Total time**: Sum of all time “spent” with that method active on the stack. E.g. it also includes time in methods called by that method.
- **Total time (CPU)**: How much CPU time the total time actually corresponded to. Like self time (CPU), a method may be active on the stack but the thread it lives on may actually be sleeping or waiting on I/O, e.g. no CPU is actually being used.

### Entry points of benchmarks
You can find source code of benchmarks under https://github.com/dacapobench/dacapobench/tree/master/benchmarks/bms

You can find the harness of benchmarks under https://github.com/dacapobench/dacapobench/blob/master/benchmarks/bms/[benchmark]/harness/src/org/dacapo/harness/[benchmark].java, take tomcat for example, https://github.com/dacapobench/dacapobench/blob/master/benchmarks/bms/tomcat/harness/src/org/dacapo/harness/Tomcat.java

You can find the entry points of benchmarks in the constrcutor, take tomcat for exmaple, the entry points is org.dacapo.tomcat.Client.run(). The source file is under https://github.com/dacapobench/dacapobench/blob/master/benchmarks/bms/tomcat/src/org/dacapo/tomcat/Client.java

Following are the complete list of entry points of benchmarks
- Tomcat:
- Eclipse:
- Xalan: org.dacapo.xalan.XSLTBench createWorkers

## Async-profiler
[Async-profiler](https://github.com/jvm-profiling-tools/async-profiler) to profile is a low overhead sampling profiler for Java that does not suffer from Safepoint bias problem. DaCapo Benchmark Suite is a widely-used Java benchmarking toolkits in the programming language, memory management and computer architecture communities. It consists of a set of open source, real world applications with non-trivial memory loads.

**However, Async-profiler is sample-based, we can not get the accurate execution time of each method. This also is the reason why we do not choose other sample-based profilers.**

```
sudo java -agentpath:[path/to/async-profiler]/build/libasyncProfiler.so=start,file=profile.html -jar dacapo-9.12-MR1-bach.jar [BenchmarkName]
```


## Alibaba Arthas
`Arthas` is a Java Diagnostic tool open sourced by Alibaba.
`Arthas` allows developers to troubleshoot production issues for Java applications without modifying code or restarting servers.

`Trace` command can track the calling path specified by class-pattern / method-pattern, and calculate the time cost on the whole path.

**However, `trace` only tracks the sub-calls of specified methods, which means we can just get one-level calling path. Other methods on the deep levels of calling path will not be tracked.**

It is because `trace` is expensive, it will insert measurement code before and after each method invocation. 


Run DaCapo Benchmark Suite first with large iteration times
```
java -jar dacapo-9.12-MR1-bach.jar -n [IterationTimes] [BenchmarkName]
```

Download and run Arthas
```
wget https://arthas.aliyun.com/arthas-boot.jar
java -jar arthas-boot.jar
```

Select correct pid to attach to jvm running DaCapo Benchmark Suite

Run `trace` command
```
trace --skipJDKMethod false [class-pattern] [method-pattern] -n [iteration_times]
```

We can find out all JDK methods we needed, and run `trace` command for each of them. Moreover, `trace` command support simple regex to track multiple methods simultaneously.
```
trace -E com.test.ClassA|org.test.ClassB method1|method2|method3
```


Before running trace for JDK methods, we need to run `option unsafe true` command to turn on instrumentation on JDK methods. However, it will break JVM in our test.
