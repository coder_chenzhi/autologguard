# Hadoop

## Build


### Clone instrumented source code

### Run Docker
```sh
./start-build-env.sh
```

### Run Test
```sh
mvn test -Dmaven.test.failure.ignore=true
### We need to ignore failed tests, because there are some flakey tests usually fail to run.
```

### Collect test logs
All logs are under `./surefire-reports` sub-dirs

```
find . -name "*-output.txt" -exec cp {} ~/hadoop_test_log/ \;
```

# Zookeeper (3.4.13)

### Clone instrumented source code

### Run test
```sh
tar zxvf zookeeper-3.4.13.tar.gz && cd zookeeper-3.4.13
```
### Run Test Coverage

```sh
ant  test-core-java
```
### Collect test logs
All log files are under `./logs`



