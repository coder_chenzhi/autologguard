# Main Process

## Identify Logging Overhead
V8
-------
语句产生的effect可以分成main effect和side effect，
一个语句是无用的，当且仅当所有的effect都没有被使用
所以我们必须获取每条语句的main effect和side effect

local main effect
local side effect
global effect，修改static field或者单例类的instance field，可以追踪，但是太麻烦，尤其是对于大型的并发系统，
所以我们简单处理，只需要记录有没有global effect，只要遇到就不能跳过
external effect，I/O操作，无法追踪，只要遇到就不能跳过

V7
-------
先不考虑方法调用，实现一个初始版本

可以使用edu.usc.sql.graph这个包的代码

topo sort可以用GlobalFlowAnalysis中的版本，但是需要自己实现一个方法，找到一个method中的back edge。

在Java里面应该只有循环结构才会产生回边。我感觉只要深度优先遍历，只要遍历到了之前访问过的节点，这个边就是回边。

需要把Soot Graph封装成我们的Graph，因为Soot中的图没有实际的边的结构，边的信息是通过节点之间的连接存储的。

V6
-------
不仅要数据流分析，还需要控制流分析，而且两种分析需要交替执行，因此不能用IFDS框架，只能手动设计算法

首先确定以statement为分析对象，而不是以变量为分析对象。变量只是存储statement的结果，而我们真正要跳过的是计算过程。

SSA中每个statement可以有多个use，但是最多只有一个define

先处理方法调用，把数据依赖图构建出来，后续的分析都基于数据依赖图，不再需要考虑方法调用的影响。
处理方法调用，就是把side effect提取出来，再构建它们与已有局部变量之间的依赖关系。
如果方法调用存在多个target，那么每个target的side effect都需要考虑进来。
如果side effect中存在全局的或外部的影响，那么可以提前停止，不需要继续看这个方法的side effect被哪些地方使用了，
因为这些影响的范围是很难分析的。因此，在构建side effect的依赖关系时，只需要构建入边，不需要构建出边，并且标记当前方法调用是不可跳过的。
而且需要约定，所有涉及到全局或外部side effect，都是不能跳过的。
考虑以下情况，第四行代码其实依赖于第三行代码，但是过程内的DefUse是发现不出来的，可能反而认为是依赖于第二行代码。
如果这样的话，考虑side effect后，还得修改过程内的DefUse信息。
```text
A a = new A();
a.f = v1;
a.setF(v2);
LOG.debug(a.f)
```
所以，方法调用实际上是将一些语句隐藏了起来，我们考虑side effect时，就是把这些语句展开，把这些语句内部的数据流信息，
跟过程内的数据流信息整合起来。这个过程中还需要修复因为信息隐藏造成的错误数据流信息。

实际上，很多方法都是有业务语义的，这些方法也是不能跳过的，而且是很强的一个提前终止条件，很多时候都是有全局/外部side effect的。
但是怎么快速判断一个方法是不是有业务语义呢？与之对应的是一些Util方法。

~~实际上，最准确的做法是先判断入参产生了哪些side effect，然后再看这些side effect是不是可以跳过的。
如果入参没有产生side effect，那么不再需要追踪side effect是如何使用的，只需要看方法调用内部是如何使用的。
如果这个入参再这个方法内部都是被日志语句使用的，那么入参的计算过程是可以跳过的，入参是可以为null的。~~
【这里不对，side effect的数据流可以通过其他方式传递给调用方，即使side effect与入参或返回值无关，也可能影响调用方的数据流】

可能要注意的地方：
- 在Java源码中变量的声明和赋值有时候是通过同一个statement完成的，但是在SSA中变量的声明和赋值都是分开的两个statement
我们是针对SSA分析，有时候会出现变量的某个赋值是可以跳过的，但是这个变量的另外一些赋值是不能跳过的，
所以这个变量的声明需要保留。
但是在映射回源码时，可能会出现在源码层面声明和赋值是同一个statement完成的，
因此需要开发人员做一些修改，将声明和赋值分开


算法：  
- 输入：  程序的SSA表示，记为P  
- 局部变量：是否到达不动点，记为Fixed  
- 输出： 可以跳过的语句集合，使用Map存储，记为skipMap，其中key为可以跳过的语句，value为对应的日志语句，初始为空  
- 过程： 
```text
1. 生成P中所有语句S的逆拓扑排序，记为worklist
2. 依次取出worklist中的语句，记为s
3. 如果s是logging calls

```



V5
-------
又发现一个问题：可能需要迭代多遍。之前的想法是，先基于数据流分析发现数据依赖相关的logging overhead，
然后再基于启发式规则，发现语义相关的logging overhead。但是，通过分析一些实例，发现执行完第二步之后，
有时候又有一些语句可以被识别为数据依赖相关的logging overhead了。

语义相关的logging overhead，我们目前只考虑控制依赖的logging overhead，如果相关

需求：
- 跨过程的数据流 / 副作用：主要是处理StringBuilder.append()
- 间接数据依赖
- 多重依赖（一个logging variable被多个logging calls依赖）

我们是否需要关心方法调用内部是如何使用参数的？还是只需要关心方法调用的结果（返回值和副作用）？

假设有一个statement，记为S1，我们要判断S1是否可以跟随logging calls一起跳过，主要看S1的结果R1是不是只被logging calls使用。
所以我们找到所有用了R1的地方，记为U1, U2, ..., Un。如果它们都是logging calls，那么s1是可以跳过的。
如果其中有些不是logging calls。假设是U1，我们接着看U1具体是怎么用的，对应的statement记为S2。
分成两种情况讨论，一种是简单运算，一种是方法调用。
先看简单运算，S2的结果记为R2，那么S1是不是可以跳过，需要看S2是不是可以跳过。如果S2可以跳过，那么S1也可以跳过。
再看方法调用，需要看方法调用内部是如何使用R1的，如果也是简单的被logging calls使用，那么S1是可以跳过的。
（这里需要注意的是，如果进一步发现S2不能跳过，那么就会出现S1可以跳过、但是使用了S1的结果的S2不能跳过，这种比较特殊的情况。
对于这种情况，需要将S1的声明和赋值分开，为了避免未初始化的错误，还需要将其初始化为null，
之后就可以将原赋值语句放到条件判断语句。）
如果并不是简单的被logging calls使用，那么需要看使用R1的地方是否是可以跳过的。
注意一种特殊情况，使用R1的结果最终会暴露给调用方（主要包括返回值和side-effect），
那么我们需要接着判断这些结果在原先的调用方中是如何被使用的。

所以，我们可以简化处理，认为方法调用的结果一定是与入参相关的。不需要再看方法调用内部具体是怎么使用入参的，
直接看return value和side effect在调用方是如何使用的。但是有时候这个步骤还可以简化，
有些方法的side effect是全局的/作用于外部的，比如业务相关操作、修改全局变量、I/O操作等等，这些方法调用肯定是不能跳过的，
所以它所使用的一些变量的计算过程也不能跳过。


总而言之，严格来说，我们需要知道方法调用内部是如何使用参数的，这样才能知道参数的数据流是否有通过返回值或side effect传递回调用方，
才能知道是不是需要接着分析方法调用的返回值和side effect是如何使用的。【这里并不对，side effect是否需要追踪，不是看返回值是否依赖于入参，
而且看side effect的类型，如果side effect可以通过全局的方式访问到，即使没有通过返回值传递出去，也可以影响调用方的数据流】
但是这个过程中存在一个快速停止条件，如果参数的数据流所产生的side effect是全局的或外部的，
那么说明参数有可能被用于其它用途，那么参数对应的变量的计算过程也就不能跳过。
我们可以简化这个过程，简单的认为方法调用的返回值和side effect都是与参数有关的。

现在的问题是，怎么能快速判断side effect是全局的或外部的？
其实大部分方法调用都是全局的或外部的，但是通常需要很多层的展开才能确定。
假设方法M1调用了方法M2，M2的入参中，有些是从M1的入参得来的，而且M1的入参是通过引用传递进来的，
那么M2的副作用可能会通过引用传播到M1之外，超出了我们的分析范围，我们可以简单的认为这个副作用是会被非日志语句使用的。


方案一：
Dominator analysis + Side-effect analysis：
- 不知道怎么处理跨过程的数据流
  - 将DefUseChain的概念扩展到side-effect，Soot好像允许创建临时JimpleLocal，
    每个语句的side-effect都新建一个JimpleLocal进行存储
- 不知道怎么处理间接数据依赖
  - 得到DefUseChain之后，获得逆拓扑排序
- 不知道怎么处理多重依赖
  - dominator analysis好像只能判断a是否被b支配，不能判断a是否被b和c支配
- 不知道side effect如何传递
  - 有些第一层的方法调用并没有side effect，需要分析更深层的方法调用才能知道side effect。
    但是，需要把所有层次的方法调用都访问一遍吗？还是只要知道有side effect就可以了？
- 不知道怎么说明结果的safety和soundness
  - 可以通过实验说明precision和recall
  - 非要分析的话，dominator analysis是flow-insensitive的

方案二：
IFDS framework:
- 不确定可以用来解决我们的问题
- 不确定算法效率怎么样
- 不知道怎么用Summary提高算法效率


IFDS天然可以处理跨过程的数据流，但是对于我们这个问题，很难说IFDS是最适合的方法，
一方面，对IFDS的很多细节模棱两可，对运行机制并不是完全清楚，
另一方面，不清楚IFDS的运行效率，不知道怎么进行优化，比如跳过一些callsite、缓存callsite的分析结果


V4
-------

使用IFDS框架，将Logging Variable Identification转化为图可达的问题求解，更准确的说，
是使用IDFS求解它的Complementary问题，也就是找non-dedicated logging variable （NDLV）

DataflowFact：保存已经确认为NDLV的JimpleLocal和对应的DefStmt，
因为好像没看到有什么方法可以从JimpleLocal找到DefStmt

InitialSeed：空集

getNormalFlowFunction：
（1）如果是JAssignStmt，先看这个Stmt中定义的DefLocal，如果DefLocal在DataflowFact中，
说明DefLocal是NDLV，那么这个Stmt中使用的UseLocal也都是NDLV，所以需要加入到DataflowFact；
如果不在DefLocal在DataflowFact中，那么DefLocal就不是NDLV，因为遍历到Local的Def时，说明这个Local的Use也都遍历过了，
还不在DataflowFact中的话，那之后肯定也不会在，所以原封不动的返回DataflowFact，好像是返回Identity.v()就行了
（2）如果是其他语句。。。（还不知道其他还有哪些需要特殊处理的语句种类）

getCallFlowFunction和getReturnFlowFunction都需要解决一个问题，判断是否要进入方法调用？

getCallFlowFunction：
跟getCallFlowFunction配对处理，做一些初始化
（1）如果是String、StringBuilder的函数调用，直接返回Identity.v()，不需要其他处理；
（2）如果是其他函数调用，主要看有没有副作用
（2.1）如果是其他Library函数调用，。。。
（2.2）如果是Native函数调用，。。。

getCallToReturnFlowFunction：
主要是处理其他没有通过函数调用传递到Callee的Dataflow Fact，感觉直接返回Identity.v()就行了

getReturnFlowFunction：
跟getCallFlowFunction配对处理，处理最终结果以及清理中间数据
（1）如果是String、StringBuilder的函数调用，直接返回Identity.v()，不需要其他处理；
（2）如果是其他函数调用，主要看有没有副作用
（2.1）如果是其他Library函数调用，。。。
（2.2）如果是Native函数调用，。。。

对于一些特殊的调用，可以提前写好处理策略，而不用再进到Callee中遍历一遍，如String、StringBuilder的函数调用
modelStringOperations


V3
-------
看起来有点像Faint Variables Analysis
首先这是一个backward data flow problem
初始状态是所有变量都是dedicated logging variable


应该在哪个级别定义？AST级别？SSA级别？分析是在SSA级别，但是最后要在AST级别优化。所以需要将SSA级别的结果映射回AST级别。

A dedicated logging variable (DLV) is a variable if along every path from the definition program point u to End,
it is only used to construct logging calls or to define dedicated logging variables.

A dedicated logging statement (DLS) is a statement if along every path from the execution program point to End,
its results are only used to construct logging calls or to define dedicated logging variables.

Statement is the minimum executable unit in the intermediate representation.

The results of instruction depends on the type of instruction. 
For assignment instructions, the results are definitions of variables. 
For invoke instructions, the results are return values and heap value writes. 


V2
-------
需要注意的地方：
设计哲学：
(1) Performance
我们希望在开发时就找到问题，所以性能必须高，必须很快的得到结果
(2) favor false negatives over false positive
意味着可以错过，但不能找错

使用什么方法构建Call Graph？
可以用最简单的RTA，意味着会找到很多impossible call site，call site越多，越有可能产生数据依赖，
就越有可能很多可以被消除的结果被检测为不能被消除

如何选择Entry Point？
1. 所有方法都作为Entry Point，构建完整的Call Graph。性能很差，而且不需要。
2. 公开API作为Entry Point。不好判断哪些是公开API，而且有可能访问不到所有包含日志代码的方法。
3. 将所有包含日志代码的方法作为Entry Point。

第三种方法看起来最合适，我们选择第三种方法。但是可能存在以下问题：
（1）没有被调用的信息，如果出现日志代码使用的变量被作为返回值的情况，
我们无法往上追溯判断这个变量的使用情况，也就无法判断返回语句是否可以被消除。但是这种情况一般都不能消除，
一方面返回值基本上都会被调用方使用，另一方面消除返回语句有时候会导致语法错误。
（2）包含日志代码的方法太多，如果其中还包含程序的主入口，整个Call Graph也会很大。
但是这种情况并不是只有第三种方法才有的问题。对于这个问题，我们可以每个方法单独运行，并行处理。


直接数据依赖和间接数据依赖

过程间的数据流带来的数据依赖

方法调用的副作用
如果所有局部变量、成员变量和静态变量的数据流都可以追踪，是否就不需要考虑副作用？
并不是，有些方法是无法追踪的，如native方法、HTTP调用、REST调用，
这些方法的执行过程对我们来说是不可见的，只能保守的认为这些方法都会影响系统的状态

多个日志输出语句的共享变量
共享变量的日志输出语句是否可以作为一个整体，在后面的性能评估过程中也放到一起？


先不考虑过程间的数据流带来的数据依赖
先不考虑多个日志输出语句的共享变量

（1）首先看最简单的情况，没有变量定义，直接作为日志输出语句的参数，如字段读取或方法调用的返回值

（2）然后看存在变量定义的情况，根据变量使用的次数分为两种情况

（2.1）变量定义之后只使用一次，且是被日志输出语句使用，这条变量定义语句是可以消除的

（2.2）变量定义之后使用过多次，再根据其中有没有用于新的变量定义，分成两种情况

（2.2.1）没有用于新的变量定义（意味着没有间接数据依赖），如果所有的USE，都被日志输出语句post-dominated，
那么暂且认为这个变量的define和use都是可以消除的；如果并不是，那么需要进一步看这些没有被日志输出语句post-dominated的USE，
是否被其他日志输出语句post-dominated

（2.2.2）用于新的变量定义（意味着存在间接数据依赖），对每一个新的变量，执行步骤（2）之后的步骤，判断新的变量定义是否可以消除，
如果所有新的变量都是可以消除的，那么这个变量定义也是可以消除的




下面是V1，已经废弃
------- 
需要被分析的日志输出语句，记为logInst

可以跳过的Inst，记为SkipInsts

首先做存活分析（Liveness Analysis），找到logInst之后存活的变量，记为liveVars

然后获取L所有使用的变量，记为usedVars

遍历usedVars中的每一个变量，记为var，检查它是否属于liveVars，

如果属于，说明这个变量在logInst之后被用过，logInst的defInst就不能被跳过，
但是defInst和logInst之间的useInst能否跳过，还需要进一步判断，因为可能存在间接使用，
只有useInst的对应的defVar以及副作用也不属于liveVars，这个useInst才能被跳过

如果不属于，则说明这个变量在logInst之后没有被用过，但是defInst和useInst能否被跳过还需要进一步判断，
因为可能存在间接使用，只有useInst的对应的defVar以及副作用也不属于liveVars，这个useInst才能被跳过



然后分析var的useInst，

如果是方法调用，需要判断是否存在副作用，如果存在副作用，需要判断副作用是否影响logInst之后的执行
如果是赋值语句，需要判断defVar的defInst和useInst是否可以跳过，需要递归执行之前的步骤


最后还有一些特殊情况需要处理：
1. IF_BLOCK, WHILE_BLOCK, FOR_BLOCK，如果内部语句都是skippable的，那么这些Block也是skippable的
2. 包含多条日志输出语句的情况，如两条相邻的日志输出语句logInst1和logInst2，logInst1中用到的vars也被logInst2用到了，
这样会导致在分析logInst1时，程序会认为vars是不能被跳过的


Input: 
- Classes: a set contains all class files
- LoggingAPIs: a set contains all logging APIs

Output: 
- Stmts: a set contains all statements belong to logging overhead

<pre>
1. for each class file:
2.   for each method invocation in each class file:
3.     if method is LoggingAPI:
4.       add all parameters of method invocation to `Stmts`
5.       for each used variable in parameters
</pre>

# TODO
- Replace direct method calls to JVM with reflection calls
- Take care of the dependency to tools.jar when shipping the java agent
- Special case: Singleton, StrongReferences, etc.