# Introduction
We test the performance of soot spark based call graph construction by using the logging entry as entry-points.

# Cassandra 
## spark default settings except closing merge-stringbuffer
2020-03-22 17:45:47 [INFO] - Start at 2020-03-22 17:45:47
2020-03-22 17:45:47 [INFO] - Start with allocated bytes 1809581792
2020-03-22 17:45:47 [INFO] - [Spark] Starting analysis ...
2020-03-22 18:04:59 [INFO] - [Spark] Done!
2020-03-22 18:04:59 [INFO] - Call graph size is 1575319
2020-03-22 18:04:59 [INFO] - Finish at 2020-03-22 18:04:59
2020-03-22 18:04:59 [INFO] - Finish with allocated bytes 314530166888
The maximum memory consumption is about 24G.

## spark default settings except closing merge-stringbuffer and closing simulate-native
2020-03-22 18:06:18 [INFO] - Start at 2020-03-22 18:06:18
2020-03-22 18:06:18 [INFO] - Start with allocated bytes 1811994032
2020-03-22 18:06:18 [INFO] - [Spark] Starting analysis ...
2020-03-22 18:10:31 [INFO] - [Spark] Done!
2020-03-22 18:10:31 [INFO] - Call graph size is 610077
2020-03-22 18:10:31 [INFO] - Finish at 2020-03-22 18:10:31
2020-03-22 18:10:31 [INFO] - Finish with allocated bytes 83087371024
The maximum memory consumption is about 12G.


# Hadoop
## spark default settings except closing merge-stringbuffer
Don't try this, because of the limit of memory size of our server

## spark default settings except closing merge-stringbuffer and closing simulate-native
2020-03-22 18:19:18 [INFO] - Start at 2020-03-22 18:19:18
2020-03-22 18:19:18 [INFO] - Start with allocated bytes 3525372640
2020-03-22 18:19:18 [INFO] - [Spark] Starting analysis ...
2020-03-22 18:30:45 [INFO] - [Spark] Done!
2020-03-22 18:30:45 [INFO] - Call graph size is 673182
2020-03-22 18:30:45 [INFO] - Finish at 2020-03-22 18:30:45
2020-03-22 18:30:45 [INFO] - Finish with allocated bytes 674785964216
The maximum memory consumption is about 16G.

