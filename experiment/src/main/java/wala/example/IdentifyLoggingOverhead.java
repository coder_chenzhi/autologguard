package wala.example;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.ibm.wala.classLoader.*;
import com.ibm.wala.ipa.callgraph.AnalysisCacheImpl;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.IAnalysisCacheView;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchyFactory;
import com.ibm.wala.ssa.*;
import com.ibm.wala.util.collections.Iterator2Iterable;
import com.ibm.wala.util.config.AnalysisScopeReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class IdentifyLoggingOverhead {

    private static final Logger LOG = LoggerFactory.getLogger(IdentifyLoggingOverhead.class);

    private static Boolean skip(IClass clazz, String projectName, Map<String, String> classpathEntries) {
        String classloader = clazz.getClassLoader().getName().toString();
        if ("Primordial".equals(classloader)) {
            return true;
        }
        String className = clazz.getName().toString();
        for (String s : Constants.FilteredPackage) {
            if (className.startsWith(s)) {
                return true;
            }
        }

        try {
            JarFileEntry moduleEntry = (JarFileEntry) ((ShrikeClass) clazz).getModuleEntry();
            String jarFile = moduleEntry.getJarFile().getName().replace("\\", "/");
            LOG.debug("Found {} in {}", clazz.toString(), jarFile);
            if (classpathEntries.containsKey(jarFile)) {
                String scope = classpathEntries.get(jarFile);
                if ("internal".equals(scope.toLowerCase())) {
                    return true;
                }
                if ("external".equals(scope.toLowerCase())) {
                    return false;
                }
                // if it belongs to mixed jar, which means this jar contains both internal classes and external classes
                // simply check whether the class' name contains project's name
                if ("mixed".equals(scope.toLowerCase())) {
                    if (clazz.toString().toLowerCase().contains(projectName.toLowerCase())) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            // if the clazz are loaded from .class file, then we will fail to get the jar file
            // we assume all these clazz are interal
            LOG.info("Throw exception when extracting the jarfile of {}", clazz.toString());
            return true;
        }

        return false;
    }

    private static Map<String, String> getClasspath(String project, Map<String, String> projectsRoot) throws IOException {
        String classpathFile = "wala/classpath/{project}_classpath.txt".replace("{project}", project.toLowerCase());
        File classPath = new File(IdentifyLoggingOverhead.class.getClassLoader().getResource(classpathFile).getFile());
        String fileContent =  Files.toString(classPath, Charsets.UTF_8);
        for (String key: projectsRoot.keySet()) {
            fileContent = fileContent.replace(key, projectsRoot.get(key));
        }
        Map<String, String> classpathEntries = new HashMap<>();
        for (String line : fileContent.split(System.lineSeparator())) {
            if (line.split(" ").length == 2) {
                classpathEntries.put(line.split(" ")[0], line.split(" ")[1]);
            }
        }
        return classpathEntries;
    }


    public static void start(String projectName, Map<String, String> projectsRoot, String outputPath) throws Exception {
        // 0. prepare logger for output
        File output = new File(String.format("%s/%s.log", outputPath, projectName));
        if (output.exists()) {
            output.delete();
            Files.touch(output);
        }

        // 1.create an analysis scope representing the source file application
        File exFile = new File(IdentifyLoggingOverhead.class.getClassLoader().getResource("wala/no_exclusion.txt").getFile());
        Map<String, String> classpathEntries = getClasspath(projectName, projectsRoot);
        String classpath = String.join(File.pathSeparator, classpathEntries.keySet());
        AnalysisScope scope = AnalysisScopeReader.makeJavaBinaryAnalysisScope(classpath, exFile);
        // 2. build a class hierarchy
        ClassHierarchy cha = ClassHierarchyFactory.make(scope);
        LOG.info(cha.getNumberOfClasses() + " classes");

        Iterator<IClass> classes = cha.iterator();
        IAnalysisCacheView cache = new AnalysisCacheImpl();

        while (classes.hasNext()) {
            IClass clazz = classes.next();
            if (skip(clazz, projectName, classpathEntries)) {
                LOG.debug("class {} has been skipped", clazz.toString());
                continue;
            }
            LOG.info("class:\t" + clazz);

            for(IMethod method: clazz.getDeclaredMethods()) {
                // skip abstract method
                if (method.isAbstract()) {
                    continue;
                }
                // no need to handle (static) field initialization code,
                // because all static initialization code are in <clinit> named method
                LOG.debug("\tmethod:\t" + method);
                try {
                    IR ir = cache.getIR(method); // can be null
                    DefUse defUse = cache.getDefUse(ir);
                    SSAInstruction[] instructions = ir.getInstructions();
                    for (int i = 0; i < instructions.length; i++) {
                        SSAInstruction instruction = instructions[i];
                        if (instruction instanceof SSAInvokeInstruction) {
                            IMethod calleeMethod = cha.resolveMethod(((SSAInvokeInstruction) instruction).getDeclaredTarget()); // can be null
                            if (calleeMethod == null) {
                                if (LOG.isDebugEnabled()) {
                                    IBytecodeMethod m = (IBytecodeMethod) ir.getMethod();
                                    int bytecodeIndex = m.getBytecodeIndex(i);
                                    int sourceLineNum = m.getLineNumber(bytecodeIndex);
                                    LOG.debug("Cannot find the declared target of method in source line {}", sourceLineNum);
                                }
                                continue;
                            }

                            IClass calleeClass = calleeMethod.getDeclaringClass();
                            String calleeClassName = calleeClass.getName().toString();
                            String calleeMethodName = calleeMethod.getName().toString();

                            doAnalysis(clazz, method, calleeClassName, calleeMethodName,
                                    instruction, ir, cha, defUse, output);
                        }
                    }
                }catch (Throwable e){
                    LOG.error("\tError while creating IR for method: " + method.getReference(), e);
                }
            }
        }

    }

    /*
    V1: for all used variables, get all statements used any of them
     */
    private static void doAnalysis(IClass callerClass, IMethod callerMethod, String calleeClassName,
                                   String calleeMethodName, SSAInstruction instruction, IR ir, ClassHierarchy cha,
                                   DefUse defUse, File output) {
        Set<SSAInstruction> skippableInsts = new HashSet<SSAInstruction>();

        String libraryName;
        // Check whether it is a logging method
        if (Constants.LoggingFunctions.containsKey(calleeClassName + "." + calleeMethodName)) {
            libraryName = Constants.LoggingFunctions.get(calleeClassName + "." + calleeMethodName);
        } else {
            return;
        }

        String jarFile = "Unknown";
        try {
            JarFileEntry moduleEntry = (JarFileEntry) ((ShrikeClass) callerClass).getModuleEntry();
            jarFile = moduleEntry.getJarFile().getName().replace("\\", "/");
        } catch (Exception e) {

        }

        int numOfUse = instruction.getNumberOfUses();
        if (numOfUse == 0) {
            LOG.warn("{}\t{}\t{}\tthe getLogger doesn't have parameters", libraryName, jarFile, callerMethod);
            return;
        }
        for (int i = 0; i < numOfUse; i++) {
            int varIndex = instruction.getUse(i);

            if (ir.getSymbolTable().isStringConstant(varIndex)) {
                // is constant
                // TODO how to handle this?
            } else {
                // is variable

                // if the defineInst and all useInst of the varIndex are skippable,
                // we can explore the used variables of defineInst
                boolean allSkippable = false;

                SSAInstruction defineInst = defUse.getDef(varIndex);
                // if only 1 use, then defineInst is skippable.

                for (SSAInstruction useInst : Iterator2Iterable.make(defUse.getUses(varIndex))) {


                }

            }
            try {
                Files.append(String.format("%s\t%s\t%s\n",
                        libraryName, jarFile, callerMethod.toString()),
                        output, Charsets.UTF_8);
//                LOG.warn(String.format("%s\t%s\t%s\t%s\t%s\t%s\n",
//                        scope, libraryName, jarFile, callerMethod.toString(), naming, loggerName));
            } catch (IOException e) {
                LOG.warn("fail to write following record to file ");
                LOG.warn("{}\t{}\t{}", libraryName, jarFile, callerMethod);
            }
        }


    }

    public static void main(String[] args) throws Exception {
        Map<String, String> projectsRoot = new HashMap<String, String>() {
            {
                put("{activemq_root}", "/home/chenzhi/Data/projects/jar/apache-activemq-5.15.8");
                put("{ambari_root}", "/home/chenzhi/Data/projects/jar/ambari-server-2.7.3.0.0-dist");
                put("{cassandra_root}", "/home/chenzhi/Data/projects/jar/apache-cassandra-3.11.3-bin/apache-cassandra-3.11.3");
                put("{flume_root}", "/home/chenzhi/Data/projects/jar/apache-flume-1.8.0-bin");
                put("{hadoop_root}", "/home/chenzhi/Data/projects/jar/hadoop-2.9.2");
                put("{hbase_root}", "/home/chenzhi/Data/projects/jar/hbase-2.1.1");
                put("{hive_root}", "/home/chenzhi/Data/projects/jar/apache-hive-3.1.1-bin");
                put("{solr_root}", "/home/chenzhi/Data/projects/jar/solr-7.5.0");
                put("{storm_root}", "/home/chenzhi/Data/projects/jar/apache-storm-1.2.2");
                put("{zookeeper_root}", "/home/chenzhi/Data/projects/jar/zookeeper-3.4.13");
                put("{JAVA_HOME}", "/usr/lib/jvm/java-8-openjdk-amd64");
                put("test_root", "");
            }
        };


        String[] projects = new String[]{"activemq", "ambari", "cassandra", "flume", "hadoop", "hbase", "hive", "solr",
                "Storm", "zookeeper"};

        projects = new String[]{"tlogserver"};
        String outputPath = "/home/chenzhi/IdeaProjects/logconfigsmelldetection/logs";

//        String prefix = "/media/chenzhi/7ae9463a-2a19-4d89-8179-d160bcb4ce1b";
//        outputPath = "" + outputPath;
//        for (String pro : projectsRoot.keySet()) {
//            projectsRoot.put(pro, prefix + projectsRoot.get(pro));
//        }

        for (String pro : projects) {
            start(pro, projectsRoot, outputPath);
        }
    }
}
