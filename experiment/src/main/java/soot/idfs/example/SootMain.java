package soot.idfs.example;

import soot.*;
import soot.options.Options;

import java.util.ArrayList;
import java.util.Arrays;

public class SootMain {

    public static void main(String[] args) {

        // Stupid code to print out the path under analysis
        String testPath = "/home/chenzhi/Code/IdeaSpace/autologguard/testcases/target/classes";
        System.out.println("Testing " + testPath);

        // Prepend classpath
        Options.v().set_soot_classpath(testPath);
        Options.v().set_prepend_classpath(true);

        Options.v().set_process_dir(Arrays.asList(testPath));

        // Enable whole-program mode
        Options.v().set_whole_program(true);
        Options.v().set_app(true);

        // Keep line number
        Options.v().set_keep_line_number(true);

        // Do not load class body for excluded classes
        Options.v().set_no_bodies_for_excluded(true);

        // Allow unresolved classes; may cause errors
        Options.v().set_allow_phantom_refs(true);

        // class output
        Options.v().set_output_format(Options.output_format_class);

        // Use Original Names
        PhaseOptions.v().setPhaseOption("jb", "use-original-names:true");

        // Call-graph options
        PhaseOptions.v().setPhaseOption("cg", "safe-newinstance:true");
        PhaseOptions.v().setPhaseOption("cg.cha", "enabled:false");

        // Enable SPARK call-graph construction
        PhaseOptions.v().setPhaseOption("cg.spark", "enabled:true");
        PhaseOptions.v().setPhaseOption("cg.spark", "verbose:true");
        PhaseOptions.v().setPhaseOption("cg.spark", "on-fly-cg:true");


        // Set EntryPoints
        ArrayList<SootMethod> entryPoints = new ArrayList<SootMethod>();
        SootClass c = Scene.v().loadClass("edu.zju.ccnt.TestLoggingOverhead", SootClass.BODIES);
        for (SootMethod method : c.getMethods()) {
            if(method.hasActiveBody()) {
                entryPoints.add(method);
            }
        }
        Scene.v().setEntryPoints(entryPoints);

        PackManager.v().getPack("wjtp").add(new Transform("wjtp.herosifds", new IFDSDataFlowTransformer()));
//        soot.Main.main(sootArgs.toArray(new String[0]));

        PackManager.v().runPacks();
    }
}
