package soot.cg;

import edu.zju.ccnt.utils.RunConfig;
import edu.zju.ccnt.utils.SootExecutorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import soot.*;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class LocalTypeTesting extends CallGraphProfiling {

    public static final Logger LOGGER = LoggerFactory.getLogger(LocalTypeTesting.class);

    public static void main(String[] args) throws Exception {
        String projectName = "cassandra";
        boolean isLocal = true;
        RunConfig runConfig = new RunConfig(isLocal);
        String classpath = runConfig.getSootClassPath(projectName);
        List<String> entryPoints = runConfig.getSootEntryPointsSigs(projectName);
        SootExecutorUtil.setDefaultSootOptions(classpath);
        SootExecutorUtil.setSootEntryPoints(entryPoints);
        SootExecutorUtil.doSparkPointsToAnalysis(Collections.singletonMap("simulate-natives","true"));

        LOGGER.info("MainClass: " + Scene.v().getMainClass().getName());

        LOGGER.info("Application classes: " + Scene.v().getApplicationClasses().size());
//        for (Iterator it = Scene.v().getApplicationClasses().iterator(); it.hasNext();) {
//            LOGGER.debug(((SootClass)it.next()).getName());
//        }

        LOGGER.info("Library classes: " + Scene.v().getLibraryClasses().size());
//        for (Iterator it = Scene.v().getLibraryClasses().iterator(); it.hasNext();) {
//            LOGGER.debug(((SootClass)it.next()).getName());
//        }

        LOGGER.info("Phantom Classes: " + Scene.v().getPhantomClasses().size());
        for (Iterator it = Scene.v().getPhantomClasses().iterator(); it.hasNext();) {
            LOGGER.debug(((SootClass)it.next()).getName());
        }


        LOGGER.info("Reachable method: {}", Scene.v().getReachableMethods().size());
        for(Iterator<?> it = Scene.v().getReachableMethods().listener(); it.hasNext();){
            SootMethod m =(SootMethod) it.next();
            LOGGER.info("Parsing method: " + m.getDeclaringClass().getName() + "." + m.getName());
            if (m.isPhantom() || m.isNative()) {
                continue;
            }
            Body body = m.retrieveActiveBody();
            for(Local l: body.getLocals()){
                Type t = l.getType();
                LOGGER.debug(t.getClass().getCanonicalName());
            }
        }
    }

}
