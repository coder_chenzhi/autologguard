package soot.cg;

import com.sun.management.ThreadMXBean;
import edu.zju.ccnt.utils.RunConfig;
import edu.zju.ccnt.utils.SootExecutorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import soot.*;

import java.lang.management.ManagementFactory;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CallGraphProfiling {
    public static final Logger LOGGER = LoggerFactory.getLogger(CallGraphProfiling.class);

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws Exception {
        boolean isLocal = false;
        String projectName = "hadoop";
        RunConfig runConfig = new RunConfig(isLocal);
        String classpath = runConfig.getSootClassPath(projectName);
        List<String> entryPoints = runConfig.getSootEntryPointsSigs(projectName);
        SootExecutorUtil.setDefaultSootOptions(classpath);
        SootExecutorUtil.setSootEntryPoints(entryPoints);

        LOGGER.info("Start at {}", LocalDateTime.now().format(dateTimeFormatter));
        ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
        long id = Thread.currentThread().getId();
        LOGGER.info("Start with allocated bytes {}", threadMXBean.getThreadAllocatedBytes(id));

        SootExecutorUtil.doSparkPointsToAnalysis(Collections.singletonMap("simulate-natives","false"));

        LOGGER.info("Call graph size is {}", Scene.v().getCallGraph().size());
        LOGGER.info("Finish at {}", LocalDateTime.now().format(dateTimeFormatter));
        LOGGER.info("Finish with allocated bytes {}", threadMXBean.getThreadAllocatedBytes(id));
    }

}
