package edu.zju.ccnt;

import edu.zju.ccnt.sootex.sideeffect.FastEscapeAnalysis;
import soot.Scene;

import java.io.File;

public class EscapeAnalysisTest {
    public static void main(String[] args) {

        String JDK = "C:/Program Files/Java/jdk1.8.0_211/jre/lib/rt.jar";
        String appClassPath = "E:\\IDEAWorkspace\\autologguards\\auto-guard\\target\\test-classes";
        String classpath = JDK + File.pathSeparator + appClassPath;
        String entry = "edu.zju.ccnt.cases.EscapeAnalysisTestCases";
        Test.loadClasses(entry, classpath, true);
        Test.doFastSparkPointsToAnalysis(false, true);

        FastEscapeAnalysis escapeAnalysis = new FastEscapeAnalysis(Scene.v().getCallGraph());
        escapeAnalysis.build();
        System.out.println("Pause");
    }

}
