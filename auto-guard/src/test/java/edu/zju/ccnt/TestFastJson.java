package edu.zju.ccnt;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import edu.zju.ccnt.guards.Unit2JDKMethods;

import java.util.ArrayList;
import java.util.List;

class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

public class TestFastJson {


    public static void serialize() {
        List<String> jdkMethods = new ArrayList<>();
        jdkMethods.add("StringBuilder.toString");
        jdkMethods.add("Integer.toString");
        Unit2JDKMethods methods = new Unit2JDKMethods("12345678-1234", "test()",
                "invokeDynamic", "123", jdkMethods);

        String jsonObject = JSON.toJSONString(methods, true);
        System.out.println(jsonObject);
    }

    public static void deserialize() {

    }

    public static void main(String[] args) {
        serialize();
    }

}
