package edu.zju.ccnt;

import edu.zju.ccnt.sootex.location.AccessPath;
import edu.zju.ccnt.sootex.sideeffect.MustAliasIdentityLocalsAnalysis;
import edu.zju.ccnt.soot.Cache;
import soot.*;
import soot.toolkits.graph.BriefUnitGraph;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MustAliasTest {

    public static void testSpecificMethod(String sig) {
        String JDK = "C:/Program Files/Java/jdk1.8.0_211/jre/lib/rt.jar;C:/Program Files/Java/jdk1.8.0_211/jre/lib/jce.jar";
        String appClassPath = "E:\\IDEAWorkspace\\autologguards\\auto-guard\\target\\test-classes";
        String classpath = JDK + File.pathSeparator + appClassPath;
        String entry = "edu.zju.ccnt.cases.AliasTestCases";
        Test.loadClasses(entry, classpath, true);
        Test.doFastSparkPointsToAnalysis(false, true);

        SootMethod sootMethod = Scene.v().getMethod(sig);
        Body body = sootMethod.retrieveActiveBody();
        MustAliasIdentityLocalsAnalysis mustAliasAnalysis =
                new MustAliasIdentityLocalsAnalysis(new BriefUnitGraph(body));
        System.out.println("Pause");
    }

    public static void normalTest() {

        String JDK = "C:/Program Files/Java/jdk1.8.0_211/jre/lib/rt.jar;C:/Program Files/Java/jdk1.8.0_211/jre/lib/jce.jar";
        String appClassPath = "E:\\IDEAWorkspace\\autologguards\\auto-guard\\target\\test-classes";
        String classpath = JDK + File.pathSeparator + appClassPath;
        String entry = "edu.zju.ccnt.cases.AliasTestCases";
        Test.loadClasses(entry, classpath, true);
        Test.doFastSparkPointsToAnalysis(false, true);

        List<?> rm = Cache.v().getTopologicalOrder();
        for (Iterator<?> it = rm.iterator(); it.hasNext();) {
            SootMethod m = (SootMethod) it.next();
            if (m.isConcrete()) {
                System.out.println("[DEBUG] " + m.getSignature() + " : " + m.getNumber());
                Body body = m.retrieveActiveBody();
                MustAliasIdentityLocalsAnalysis mustAliasAnalysis =
                        new MustAliasIdentityLocalsAnalysis(new BriefUnitGraph(body));
                for (Local local : body.getLocals()) {
                    if (local.getType() instanceof RefLikeType) {
                        boolean flag = mustAliasAnalysis.isMustAliasToIdentityLocalAtExit(local);
                        System.out.println("Local: " + local + " " + flag);
                        if (flag) {
                            Set<AccessPath> aps = mustAliasAnalysis.getMustAliasToIdentityLocalAtExit(local);
                            System.out.println(aps);
                        }
                    }
                }
            }
        }
        System.out.println("Pause");
    }

    public static void main(String[] args) {
        testSpecificMethod("<java.util.TreeMap: java.lang.Object put(java.lang.Object,java.lang.Object)>");
    }

}
