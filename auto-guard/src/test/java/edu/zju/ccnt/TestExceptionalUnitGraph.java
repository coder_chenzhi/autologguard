package edu.zju.ccnt;

import soot.Body;
import soot.Scene;
import soot.SootMethod;
import soot.toolkits.graph.BriefUnitGraph;
import soot.toolkits.graph.ExceptionalUnitGraph;

import java.io.File;

public class TestExceptionalUnitGraph {

    public static void main(String[] args) {
        String JDK = "C:/Program Files/Java/jdk1.8.0_211/jre/lib/rt.jar";
        String appClassPath = "E:\\IDEAWorkspace\\autologguards\\auto-guard\\target\\test-classes";
        String classpath = JDK + File.pathSeparator + appClassPath;
        String entry = "edu.zju.ccnt.cases.EscapeAnalysisTestCases";
        Test.loadClasses(entry, classpath, true);
        SootMethod sootMethod = Scene.v().getMethod("<java.lang.Integer: java.lang.Integer decode(java.lang.String)>");
        Body body = sootMethod.retrieveActiveBody();
        BriefUnitGraph briefUnitGraph = new BriefUnitGraph(body);
        ExceptionalUnitGraph exceptionalUnitGraph = new ExceptionalUnitGraph(body);
        System.out.println("Pause");
    }

}
