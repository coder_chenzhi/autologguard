package edu.zju.ccnt.cases;

public class AliasTestCases {

    String this1;

    public static void main(String[] args) {
        AliasTestCases testCases = new AliasTestCases();
        Person person = new Person("Daniel", false, 12);
        testCases.testThisLocal();
        testCases.testParametersLocal("test");
        testCases.testFieldAccess(person);
        testCases.testBranch();
        testCases.testBranch("a", "b");
    }

    void testThisLocal() {
        String tmp = this1;
        String result = tmp;
        System.out.println(result);
    }

    void testParametersLocal(String para1) {
        String tmp = para1;
        String result = tmp;
        System.out.println(result);
    }

    void testFieldAccess(Person person) {
        String name = person.name;
        System.out.println(name);
    }

    void testBranch() {
        String tmp = this1;
        String result;
        if (tmp.equals("test")) {
            result = "test";
        } else {
            result = tmp;
        }
        System.out.println(result);
    }

    void testBranch(String para1, String para2) {
        String result;
        if (para1.length() > para2.length()) {
            result = para1;
        } else {
            result = para2;
        }
        System.out.println(result);
    }

}
