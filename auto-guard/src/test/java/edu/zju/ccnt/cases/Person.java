package edu.zju.ccnt.cases;


public class Person {

    public static String FORMAT = "name: %s, isMale: %b, age: %i";

    String name;
    Boolean isMale;
    Integer age;

    public Person(String name, Boolean isMale, Integer age) {
        this.name = name;
        this.isMale = isMale;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getMale() {
        return isMale;
    }

    public void setMale(Boolean male) {
        isMale = male;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

}