package edu.zju.ccnt.cases;

public class SideEffectTestCases {

    public static void main(String[] args) {
        Person p1 = new Person("Zhang San", true, 18);
        Person p2 = new Person("Zhang San", true, 18);
        testStringAppend();
        testStringFormat("test");
        changeName(p1, "Li Si");
        exchangeName(p1, p2);
    }

    // expectation:
    // void testStringAppend() does not have side effect on this local or parameter locals
    // void testStringAppend() has unskippable side effect, because it calls i/o method
    public static void testStringAppend() {
        StringBuilder sb = new StringBuilder();
        sb.append("This").append("is").append("a").append("test");
        System.out.println(sb.toString());
    }

    // expectation:
    // void testStringFormat() does not have side effect on this local or parameter locals
    // void testStringFormat() has unskippable side effect, because it calls i/o method
    public static void testStringFormat(String str) {
        String format = String.format("This is a %s", str);
        System.out.println(format);
    }

    // expectation:
    // mod: [person.name]
    public static void changeName(Person person, String newName) {
        person.setName(newName);
    }

    // expectation:
    // mod: [p1.name, p2.name]
    // use: [p1.name, p2.name]
    public static void exchangeName(Person p1, Person p2) {
        String name = p1.getName();
        p1.setName(p2.getName());
        p2.setName(name);
    }

}
