package edu.zju.ccnt.cases;

public class EscapeAnalysisTestCases {

    public static void main(String[] args) {
        Person p = new Person("Zhang San", true, 18);
        String str = "test";
        testStoreStaticField(str);
        testStoreInstanceField(str);
        testStoreInstanceFieldBySetter(str);
        testStoreInstanceField(p, str);
        testStoreInstanceFieldBySetter(p, str);

        testStringAppend();
        testStringFormat("test");
    }

    // expectation: this1 is escaped, and this method is escaped
    public static String testStoreStaticField(String str) {
        Person.FORMAT = str;
        return str;
    }

    // expectation: this1 is escaped, and this method is not escaped
    public static String testStoreInstanceField(Person p, String str) {
        p.name = str;
        return str;
    }

    // expectation: this1 is escaped, and this method is not escaped
    public static String testStoreInstanceFieldBySetter(Person p, String str) {
        p.setName(str);
        return str;
    }

    // in current implementation, this1 is escaped, and this method is escaped
    public static String testStoreInstanceField(String str) {
        Person p = new Person("Zhang San", true, 18);
        p.name = str;
        return str;
    }

    // in current implementation, this1 is escaped, and this method is escaped
    public static String testStoreInstanceFieldBySetter(String str) {
        Person p = new Person("Zhang San", true, 18);
        p.name = str;
        return str;
    }


    // expectation: none is escaped
    public static void testStringAppend() {
        StringBuilder sb = new StringBuilder();
        sb.append("This").append("is").append("a").append("test");
        System.out.println(sb.toString());
    }

    // expectation: none is escaped
    public static void testStringFormat(String str) {
        String format = String.format("This is a %s", str);
        System.out.println(format);
    }


}
