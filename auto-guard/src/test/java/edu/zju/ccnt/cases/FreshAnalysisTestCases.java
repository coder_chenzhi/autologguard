package edu.zju.ccnt.cases;

public class FreshAnalysisTestCases {

    public static void main(String[] args) {
        testGetter();
        testSetter();
        testGetterAndSetter();
        testGetterAndSetter(new Person("Zhang San", true, 18));
        testStringAppend();
        testStringFormat("test");
    }

    // expectation: p is fresh
    public static void testGetter() {
        Person p = new Person("Zhang San", true, 18);
        p.getName();
    }

    // expectation: p is fresh
    public static void testSetter() {
        Person p = new Person("Zhang San", true, 18);
        p.setName("Li Si");
    }

    // expectation: person and name are fresh
    // Obviously, the variable person is fresh.
    // As for the variable name, person.getName is fresh method, but the return value of person.getName() contains a  it is depend on variable person.
    public static void testGetterAndSetter() {
        Person person = new Person("Zhang San", true, 18);
        String name = person.getName();
        person.setName(name);
    }

    // expectation: person and name are not fresh
    public static void testGetterAndSetter(Person person) {
        String name = person.getName();
        person.setName(name);
    }

    // expectation: sb is fresh
    public static void testStringAppend() {
        StringBuilder sb = new StringBuilder();
        sb.append("This").append("is").append("a").append("test");
        System.out.println(sb.toString());
    }

    // expectation: format is fresh, this1 is non-fresh
    public static void testStringFormat(String str) {
        String format = String.format("This is a %s", str);
        System.out.println(format);
    }

}
