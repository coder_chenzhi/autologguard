package edu.zju.ccnt.cases;

import java.util.UUID;

public class TestID {

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            UUID uuId = UUID.nameUUIDFromBytes(("Test" + i % 10).getBytes());
            System.out.println(uuId);
        }
    }

}
