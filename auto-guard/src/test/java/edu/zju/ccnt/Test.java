package edu.zju.ccnt;
import java.io.*;
import java.util.*;

import edu.zju.ccnt.soot.SootUtils;
import edu.zju.ccnt.soot.Utils;
import edu.zju.ccnt.soot.callgraph.CallGraphRefiner;
import soot.*;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.options.Options;


/**
 * @author bruteforce
 *
 */
public class Test {
    public static PrintStream out = System.out;

    public static void setDefaultSootOptions(String classpath, boolean isWholeProgramAnalysis, boolean inShimple){
        String outputDir = "./output/soot";

        File file = new File(outputDir);
        file.mkdirs();
        //clearDirectory(outputDir);


        if(isWholeProgramAnalysis){
            if(inShimple){
                Options.v().set_whole_shimple(true);
                Options.v().set_via_shimple(true);
            }
            else{
                Options.v().set_whole_program(isWholeProgramAnalysis);
            }
        }

        Options.v().set_verbose(true);
        Options.v().set_soot_classpath(classpath);
        Options.v().set_output_dir(outputDir);
        Options.v().set_output_format(Options.output_format_none);
        Options.v().set_keep_line_number(true);
        Options.v().set_no_bodies_for_excluded(false);
        Options.v().set_include_all(true);
        Options.v().set_allow_phantom_refs(true);
        //Options.v().print_tags_in_output();
        Options.v().set_print_tags_in_output(true);
        Options.v().setPhaseOption("jb", "use-original-names:true");
        PhaseOptions.v().setPhaseOption("bb", "enabled:false");
        PhaseOptions.v().setPhaseOption("jb", "use-original-names:true");
        //Options.v().set_validate(true);
    }

    public static Collection<SootClass> loadClasses(String entryClass, String classpath, boolean isWholeProgramAnalysis){
        Date startTime = new Date();

        setDefaultSootOptions(classpath, isWholeProgramAnalysis, false);

        //load classes for analysis
        SootUtils.loadClassesForEntry(entryClass);

        Date endTime=new Date();
        Test.out.println("Load " + Scene.v().getClasses().size() + " soot classes in "+ Utils.getTimeConsumed(startTime,endTime));

        SootUtils.numberClassAndFields();
        return Scene.v().getClasses();
    }

    public static void doFastSparkPointsToAnalysis(boolean usePoem, boolean refineCG) {
        Map<String,String> opt = new HashMap<String,String>();
        opt.put("simulate-natives","false");
        opt.put("implicit-entry","false");
        if (usePoem) {
            SootUtils.doGeomPointsToAnalysis(opt);
        } else {
            SootUtils.doSparkPointsToAnalysis(opt);

        }
        if (refineCG) {
            // simplify call graph, ignore method not reachable from main entry
            // ignore implicit calls (except thread calls)
            CallGraph cg = Scene.v().getCallGraph();
            PointsToAnalysis ptsTo = Scene.v().getPointsToAnalysis();
            CallGraphRefiner refiner = new CallGraphRefiner(ptsTo, false);
            CallGraph newCg = refiner.refine(cg, new CallGraphRefiner.AggressiveCallGraphFilter());
            Scene.v().setCallGraph(newCg);
            Scene.v().setReachableMethods(null);   //update reachable methods
        }
    }

}
