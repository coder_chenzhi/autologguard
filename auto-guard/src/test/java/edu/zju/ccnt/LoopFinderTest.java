package edu.zju.ccnt;

import edu.zju.ccnt.guards.Loop;
import edu.zju.ccnt.guards.LoopFinder;
import edu.zju.ccnt.soot.hammock.CFGProvider;
import edu.zju.ccnt.soot.hammock.HammockCFG;
import edu.zju.ccnt.soot.hammock.HammockCFGProvider;
import edu.zju.ccnt.utils.RunConfig;
import edu.zju.ccnt.utils.SootExecutorUtil;
import soot.Body;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;

import java.util.Set;

public class LoopFinderTest {

    public static void main(String[] args) throws Exception {
        boolean isLocal = false;
        String projectName = "cassandra";
        String className = "org.apache.cassandra.db.compaction.LeveledManifest";
        String methodName = "<org.apache.cassandra.db.compaction.LeveledManifest: java.util.Collection getOverlappingStarvedSSTables(int,java.util.Collection)>";
        RunConfig runConfig = new RunConfig(isLocal);
        String classpath = runConfig.getSootClassPath(projectName);
        SootExecutorUtil.setDefaultSootOptions(classpath);

        SootClass clazz = Scene.v().loadClass(className, SootClass.BODIES);
        Scene.v().loadNecessaryClasses();

//        SootMethod method = clazz.getMethod(methodName);
        for (SootMethod method : clazz.getMethods()) {
            if (method.getSignature().equals(methodName)) {
                Body body = method.retrieveActiveBody();
                Set<Loop> loops = LoopFinder.getLoops(new HammockCFG(body));
                System.out.println(loops);
            }
        }
    }

}
