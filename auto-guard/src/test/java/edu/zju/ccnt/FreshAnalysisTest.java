package edu.zju.ccnt;

import edu.zju.ccnt.sootex.sideeffect.FreshAnalysis;
import soot.Scene;

import java.io.File;

public class FreshAnalysisTest {

    public static void main(String[] args) {
        String JDK = "C:/Program Files/Java/jdk1.8.0_211/jre/lib/rt.jar";
        String appClassPath = "E:\\IDEAWorkspace\\autologguards\\auto-guard\\target\\test-classes";
        String classpath = JDK + File.pathSeparator + appClassPath;
        String entry = "edu.zju.ccnt.cases.FreshAnalysisTestCases";
        Test.loadClasses(entry, classpath, true);
        Test.doFastSparkPointsToAnalysis(false, true);

        FreshAnalysis freshAnalysis = new FreshAnalysis(Scene.v().getCallGraph());
        freshAnalysis.build();
        System.out.println("Pause");
    }
}
