package edu.zju.ccnt;

import static org.junit.Assert.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Unit test for simple App.
 */
public class PaperDemoCode {

    ByteBuffer buffer;

    protected ReentrantReadWriteLock.ReadLock readLock;

    final Map<String, Node>
            scheduler = new ConcurrentHashMap<>();

    public static final Logger LOGGER = LoggerFactory.getLogger("test");

    public static void main(String[] args) {
        ArrayList<String> strs = new ArrayList<>();
        strs.add("this");
        strs.add("is");
        System.out.println(strs.toString());
    }

    void readConnectResult() {
        if (LOGGER.isTraceEnabled()) {
            StringBuilder sb = new StringBuilder("0x[");
            for (byte b : buffer.array()) {
                sb.append(Integer.toHexString(b)).append(",");
            }
            sb.append("]");
            LOGGER.trace("readConnectResult" + buffer.remaining()
                    + ":" + sb.toString());
        }
    }


    void logDestroyNode(NodeContext nc) {
        StringBuilder sb = new StringBuilder();
        NodeStatus ns = checkDestroyStatus(nc);
        if (ns == NodeStatus.WAIT_APP) {
            sb.append(String.format(
                    " timeout:%4ds", getTimeoutInSec(nc)));
        }
        Iterator<App> it;
        for (it = nc.getApps().iterator(); it.hasNext(); ) {
            App app = it.next();
            sb.append(String.format(
                    " appID: %s", app.getId()));
        }
        LOGGER.debug("Destroy node: " + sb.toString());
    }

    class Node {

        int usedMem;
        int remainMem;

        String summary() {
            return "";
        }

    }

    enum NodeStatus {
        WAIT_APP,
        WAIT_CONTAINER
    }

    class NodeContext {
        private List<App> apps;

        public List<App> getApps() {
            return apps;
        }
    }

    class App {
        String id;
        String state;
        double progress;

        public String getId() {
            return id;
        }

        public String getState() {
            return state;
        }

        public double getProgress() {
            return progress;
        }
    }

    static NodeStatus checkDestroyStatus(NodeContext nc) {
        return NodeStatus.WAIT_APP;
    }

    static long getTimeoutInSec(NodeContext node) {
        return 1;
    }

    class Record {

    }

    class TxnHeader {

        public OpCode getType() {
            return OpCode.create;
        }


    }

//    public void processTxn(TxnHeader header, Record txn) {
//        String debug = "";
//        try {
//            switch (header.getType()) {
//                case OpCode.create:
//                    CreateTxn createTxn = (CreateTxn) txn;
//                    debug = "Create transaction for " + createTxn.getPath();
//                    // TODO
//                case OpCode.multi:
//                    MultiTxn multiTxn = (MultiTxn) txn;
//                    List<Txn> txns = multiTxn.getTxns();
//                    debug = "Multi transaction with " + txns.size() + " operations";
//                    // TODO
//                    break;
//                default:
//                    throw new IllegalStateException("Unexpected value: " + header.getType());
//            }
//        } catch (IOException e) {
//            if (LOGGER.isDebugEnabled()) {
//                // LOGGER.debug("{}: {} failed: {}", header.getType(), debug, txn, e);
//                LOGGER.debug(header.getType() + ": "  + debug + " failed: " + txn, e);
//            }
//        }
//        // TODO
//    }

    private enum OpCode {
        create,
        multi
    }

    private class CreateTxn extends Record{
        public String getPath() {
            return null;
        }
    }

    class Txn {

    }

    private class MultiTxn extends Record{
        public List<Txn> getTxns() {
            return null;
        }
    }

    public void showScheduledNode(App app) {
        if (LOGGER.isDebugEnabled()) {
            readLock.lock();
            Node node = scheduler.get(app.getId());
            LOGGER.debug("Application {} is scheduled to Node {}",
                    app.getId(), node.summary());
            readLock.unlock();
        }
    }
}

