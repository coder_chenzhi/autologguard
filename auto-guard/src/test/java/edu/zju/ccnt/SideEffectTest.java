package edu.zju.ccnt;

import edu.zju.ccnt.sootex.ptsto.IPtsToQuery;
import edu.zju.ccnt.sootex.ptsto.SparkPtsToQuery;
import edu.zju.ccnt.sootex.sideeffect.SideEffectAnalysis;
import edu.zju.ccnt.utils.Constants;
import soot.Scene;

import java.io.File;

public class SideEffectTest {

    public static void main(String[] args) {
        String JDK = "C:/Program Files/Java/jdk1.8.0_211/jre/lib/rt.jar";
        String appClassPath = "E:\\IDEAWorkspace\\autologguards\\auto-guard\\target\\test-classes";
        String classpath = JDK + File.pathSeparator + appClassPath;
        String entry = "edu.zju.ccnt.cases.SideEffectTestCases";
        Test.loadClasses(entry, classpath, true);
        Test.doFastSparkPointsToAnalysis(false, true);

        IPtsToQuery ptsto = new SparkPtsToQuery();

        SideEffectAnalysis sideEffectAnalysis = new SideEffectAnalysis(ptsto, Scene.v().getEntryPoints(),
                    Constants.CUSTOMIZED_IO_METHOD);
        sideEffectAnalysis.build();
        System.out.println("Pause");
    }

}
