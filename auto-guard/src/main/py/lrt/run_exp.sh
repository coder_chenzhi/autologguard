# 
#		0: MART (gradient boosted regression tree)
# 		1: RankNet
# 		2: RankBoost
# 		3: AdaRank
# 		4: Coordinate Ascent
# 		6: LambdaMART
# 		7: ListNet
# 		8: Random Forests
#

for id in 0 1 2 3 4 6 7 8
do
	echo $id
	# Fold 1
	cat zookeeper-data/folds/fold1 zookeeper-data/folds/fold2 zookeeper-data/folds/fold3 zookeeper-data/folds/fold4 > zookeeper.train
	cat zookeeper-data/folds/fold5 > zookeeper.test
	java -jar RankLib-2.15.jar -train zookeeper.train -test zookeeper.test -feature 5 -ranker $id -metric2t NDCG@30 -metric2T P@30

	# Fold 2
	cat zookeeper-data/folds/fold1 zookeeper-data/folds/fold2 zookeeper-data/folds/fold3 zookeeper-data/folds/fold5 > zookeeper.train
	cat zookeeper-data/folds/fold4 > zookeeper.test
	java -jar RankLib-2.15.jar -train zookeeper.train -test zookeeper.test -feature 5 -ranker $id -metric2t NDCG@30 -metric2T P@30

	# Fold 3
	cat zookeeper-data/folds/fold1 zookeeper-data/folds/fold2 zookeeper-data/folds/fold4 zookeeper-data/folds/fold5 > zookeeper.train
	cat zookeeper-data/folds/fold3 > zookeeper.test
	java -jar RankLib-2.15.jar -train zookeeper.train -test zookeeper.test -feature 5 -ranker $id -metric2t NDCG@30 -metric2T P@30

	# Fold 4
	cat zookeeper-data/folds/fold1 zookeeper-data/folds/fold3 zookeeper-data/folds/fold4 zookeeper-data/folds/fold5 > zookeeper.train
	cat zookeeper-data/folds/fold2 > zookeeper.test
	java -jar RankLib-2.15.jar -train zookeeper.train -test zookeeper.test -feature 5 -ranker $id -metric2t NDCG@30 -metric2T P@30

	# Fold 5
	cat zookeeper-data/folds/fold2 zookeeper-data/folds/fold3 zookeeper-data/folds/fold4 zookeeper-data/folds/fold5 > zookeeper.train
	cat zookeeper-data/folds/fold1 > zookeeper.test
	java -jar RankLib-2.15.jar -train zookeeper.train -test zookeeper.test -feature 5 -ranker $id -metric2t NDCG@30 -metric2T P@30
done