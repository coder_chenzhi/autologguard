package edu.zju.ccnt.sootex.location;


public enum HeapAbstraction{ 
	FIELD_SENSITIVE, FIELD_BASED, TYPE_BASED, NO_DISTINGUISH;
}