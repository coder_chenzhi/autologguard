package edu.zju.ccnt.sootex.ptsto;

public enum PointsToAnalysisType{   
	SPARK, TYPE_BASED, NAIVE
}