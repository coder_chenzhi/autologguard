package edu.zju.ccnt.sootex.ptsto;

import java.util.*;

import edu.zju.ccnt.sootex.location.InstanceObject;
import edu.zju.ccnt.sootex.location.Location;
import edu.zju.ccnt.sootex.location.UnknownArraySpace;
import edu.zju.ccnt.sootex.location.UnknownInstObject;
import soot.*;


/**
 * A dumb points-to relation query by type.
 * Model heap by an unknown object and an unknown array
 */
public class NaivePtsToQuery implements IPtsToQuery{
	public NaivePtsToQuery(){ }  
    
    private Set<InstanceObject> getPointTos(Type type){
    	Set<InstanceObject> pt2Set = new HashSet<InstanceObject>();
    	
    	if(type.equals(Scene.v().getObjectType())){
    		// can point to every kind of object
    		pt2Set.add(UnknownArraySpace.v());
    		pt2Set.add(UnknownInstObject.v());
    	}
    	else if(type instanceof ArrayType){
    		pt2Set.add(UnknownArraySpace.v());
    	}
    	else{
    		pt2Set.add(UnknownInstObject.v());
    	}   
    	
    	return pt2Set;
    }
    
    public Set<InstanceObject> getPointTos(SootMethod m, Unit stmt, Location ptr){
    	if(!ptr.isPointer())
    		return Collections.emptySet();
    	
    	Type type = ptr.getType();
    	return getPointTos(type);
    }
}