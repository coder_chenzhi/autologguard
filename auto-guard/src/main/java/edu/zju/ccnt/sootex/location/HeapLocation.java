package edu.zju.ccnt.sootex.location;


/**
 * <immutable>
 */
public abstract class HeapLocation extends Location{  	
	public abstract InstanceObject getWrapperObject();
} 