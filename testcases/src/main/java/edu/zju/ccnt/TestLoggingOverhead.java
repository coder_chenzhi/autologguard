package edu.zju.ccnt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class TestLoggingOverhead {

    private static final Logger LOG = LoggerFactory.getLogger(TestLoggingOverhead.class);

    private String str;

    private String expensiveCalls() {
        Random random = new Random();
        return Long.toString(random.nextLong());
    }

    private void testStringLiteral() {
        LOG.debug("StringLiteral");
    }

    private void testStringLiteralConcat() {
        LOG.debug("String" + "Literal" + "Concat");
    }

    private void testVariable_01() {
        String str = "variable";
        LOG.debug(str);
    }

    private void testVariable_02(String str) {
        LOG.debug(str);
    }

    private void testVariable_03() {
        String str;
        str = "post read";
        LOG.debug(str);
        System.out.println(str);
    }

    private void testVariable_04() {
        String str;
        str = "variable";
        LOG.debug(str);
        str = "post write";
        System.out.println(str);
    }

    private void testVariable_05() {
        String str1 = "hello";
        String str2 = "world";
        str2.concat(str1);
        LOG.debug("{}, {}!", str1, str2);
    }

    private void testBranch_01() {
        Person person = new Person("Victor", true, 25);
        String gender;
        if (person.getGender()) {
            gender = "male";
        } else {
            gender = "female";
        }
        LOG.debug(gender);
    }

    private void testBranch_02() {
        Person person = new Person("Victor", true, 25);
        String gender;
        if (person.getGender()) {
            gender = "male";
        } else {
            gender = "female";
        }
        LOG.debug(gender);
        System.out.println(gender);
    }

    private void testBranch_03() {
        Person person = new Person("Victor", true, 25);
        String gender;
        String title = "Mr.";
        if (person.getGender()) {
            str = "male";
        } else {
            str = "female";
            title = "Mrs.";
        }
        LOG.debug(str);
        System.out.println(title);
    }

    private void testBranch_04() {
        String title;
        title = "Mrs.";
        LOG.debug(title);
        Person person = new Person("Victor", true, 25);
        if (person.getGender()) {
            str = "Mr.";
        }
        System.out.println(str);
    }

    private void testNoSideEffectMethodCall_01() {
        Person person = new Person("Victor", true, 25);
        LOG.debug(person.getName());
    }

    private void testNoSideEffectMethodCall_02() {
        Person person = new Person("Victor", true, 25);
        LOG.debug(person.toString());
    }

    private void testNoSideEffectMethodCall_03() {
        Person person = new Person("Victor", true, 25);
        // The Person.increasesAge() changes the age of Person,
        // but the age of Person is not used in other places
        // TODO Pay attention to Singleton class
        Integer age = person.increaseAge();
        LOG.debug(age.toString());
    }

    private void testSideEffectMethodCall_01() {
        Person person = new Person("Victor", true, 25);
        int step = 12;
        // The Person.increasesAge() changes the age of Person,
        // and the age of Person is used in other places
        Integer age = person.increaseAge(step);
        LOG.debug(age.toString());
        System.out.println(person.getAge());
    }

    private void testSideEffectMethodCall_02() {
        Person person = new Person("Victor", true, 25);
        // The Person.changeDefaultCountry() changes the default country.
        // It is static field of class Person, which can be accessed by any other place.
        person.changeDefaultCountry("USA");
        LOG.debug(person.toString());
    }

    private void testSideEffectMethodCall_03() {
        Person man = new Person("Victor", true, 25);
        Person woman = new Person("Victoria", true, 25);
        // The Person.setCouple() changes the couple field of caller object and parameter object.
        man.setCouple(woman);
        LOG.debug(man.toString());
        System.out.println(woman.getName());
    }


    private void testSideEffectMethodCall_04_Caller() {
        String str = expensiveCalls(); // skipable by following code
//        String str = null;
//        if (LOG.isDebugEnabled()) {
//            str = expensiveCalls();
//        }
        testSideEffectMethodCall_04_Callee(str);
    }

    private void testSideEffectMethodCall_04_Callee(String str) {
        LOG.debug(str);
    }


}
