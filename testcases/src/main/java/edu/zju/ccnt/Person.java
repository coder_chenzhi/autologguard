package edu.zju.ccnt;

class Person {

    public static String defaultCountry = "China";

    String name;
    Boolean gender;
    Integer age;
    Person couple;

    public Person(String name, Boolean gender, Integer age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    /**
     * no side effect
     * @return
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * no side effect
     * @return
     */
    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    /**
     * no side effect
     * @return
     */
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer increaseAge() {
        return ++this.age;
    }

    public Integer increaseAge(int step) {
        this.age += step;
        return this.age;
    }

    public String changeDefaultCountry(String newCountry) {
        Person.defaultCountry = newCountry;
        return Person.defaultCountry;
    }

    public Person getCouple() {
        return couple;
    }

    public void setCouple(Person other) {
        this.couple = other;
        other.couple = this;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                '}';
    }
}
