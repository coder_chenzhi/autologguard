package edu.zju.ccnt.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.management.ThreadMXBean;
import java.lang.management.ManagementFactory;


class Person {
    Integer age;
}

public class Main {
    // public static final Log OVERHEAD_LOGGER = LogFactory.getLog("LoggingOverhead");
    public static final Logger OVERHEAD_LOGGER = LoggerFactory.getLogger("LoggingOverhead");

    /**
     * ThreadMXBean.getThreadAllocatedBytes(id) reports the cumulative amount of bytes allocated in heap
     * on behalf of the target thread, which is roughly equivalent to the size of the objects allocated.
     * It means that reclamation of heap space has no effect on the reported values.
     * Therefore, the value is not equivalent to the actual memory consumption.
     *
     * More details can be found in below SO question
     * https://stackoverflow.com/questions/36176593/will-threadmxbeangetthreadallocatedbytes-return-size-of-allocated-memory-or-obj
     *
     */
    public static void memUsage() {
        // put below code snippet at the entry of each methods containing logging overheads
        ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
        long _threadID = Thread.currentThread().getId();
        long beforeMemUsage = 0;
        long afterMemUsage = 0;
        long beforeTime = 0;
        long afterTime = 0;
        String loggingID = "";

        // put below code snippet before logging overheads exactly
        // Note!!! assign correct logging ID to loggingID variable


        loggingID = "";
        beforeMemUsage = threadMXBean.getThreadAllocatedBytes(_threadID);
        beforeTime = System.nanoTime();

        {
            // put the code you want to measure here
            for (int i = 0; i < 30; i++) {
                new Long(i);
            }
        }

        // put below code snippet after logging overheads exactly
        // measure time before memory, because the call to System.nanoTime() will not incur memory allocation,
        // but the call to ThreadMXBean.getThreadAllocatedBytes(_threadID) will increase execution time.


        afterTime = System.nanoTime();
        afterMemUsage = threadMXBean.getThreadAllocatedBytes(_threadID);
        OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));


        afterTime = System.nanoTime();
        afterMemUsage = threadMXBean.getThreadAllocatedBytes(_threadID);
        OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: " + loggingID +
                ", Time consumption: " + (afterTime - beforeTime)
                + " ns, Space consumption: " + (afterMemUsage - beforeMemUsage) + " bytes");


    }

    public static void main(String[] args) {
        memUsage();
    }

}
