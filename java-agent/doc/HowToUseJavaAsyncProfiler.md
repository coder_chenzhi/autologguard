# Prerequisites
Download the [async-profiler](https://github.com/jvm-profiling-tools/async-profiler) from Github releases. 
Unzip the tar file.
# Java Agent
Following is the test code:
```java
public class ProfileHeapAlloc {
    public static void main(String[] args) {
        new Long(0);
    }
}
```
Compile the test code:
```shell script
javac ProfileHeapAlloc.java
```
Run the profiler:
```shell script
java -XX:+UnlockDiagnosticVMOptions -XX:+DebugNonSafepoints -agentpath:"/path/to/async-profiler/build/libasyncProfiler.so=start,event=alloc,file=allocation-flame-graph.svg,svg,title=Allocation profile,width=1600" ProfileHeapAlloc
```

# Programmatically
Following is the test code:
```java
import one.profiler.AsyncProfiler;

public class ProfileHeapAlloc {

    public static void main(String[] args) throws Exception {
        AsyncProfiler profiler = AsyncProfiler.getInstance();

        // Dry run to skip allocations caused by AsyncProfiler initialization
        profiler.start("_ZN13SharedRuntime19dtrace_object_allocEP7oopDesci", 0);
        profiler.stop();

        // Real profiling session
        profiler.start("_ZN13SharedRuntime19dtrace_object_allocEP7oopDesci", 0);

        new Long(0);

        profiler.stop();
        profiler.execute("file=alloc.svg");
    }
}
```
Compile the test code:
```shell script
javac -cp .:/path/to/async-profiler/build/async-profiler.jar ProfileHeapAlloc.java
```
Run the profiler:
```shell script
sudo java -cp .:/path/to/async-profiler/build/async-profiler.jar -Djava.library.path=/path/to/async-profiler/build/ -XX:+DTraceAllocProbes  ProfileHeapAlloc
```