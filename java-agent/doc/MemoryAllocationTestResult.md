# 测试环境1
```
java version "1.8.0_131"
Java(TM) SE Runtime Environment (build 1.8.0_131-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)
```

## Long
```
0 Long	48
1 Long	456
10 Long	672
20 Long	912
30 Long 1152
```
从1->10, 10->20, 20->30这几个增量，可以看出每个Long占用24个字节，包括12字节的Object Header、8字节的long以及4字节的字段间内存对齐

但是不知道如何解释从0->1的这个增量。根据目前的实验，应该是与类加载有关的内存分配，所以只在第一次使用这个类时进行内存分配，跟对象的数量没有关系。在实际情况下，大部分的类在执行日志语句之前都使用过，所以也可以忽略不记。

```
0 Long	48
1 Long	72
10 Long	288
20 Long	528
30 Long 768
```

## Integer
```
0 Integer	48
1 Integer	64
10 Integer	208
20 Integer	368
30 Integer	528
```
从1->10, 10->20, 20->30这几个增量，可以看出每个Integer Object占用16个字节，包括12字节的Object Header和4字节的int


## Boolean
```
0 Boolean	48
1 Boolean	480
10 Boolean	624
20 Boolean	784
30 Boolean	944
```
从1->10, 10->20, 20->30这几个增量，可以看出每个Boolean Object占用16个字节，包括12字节的Object Header、1字节的bool和3字节的对象间内存对齐


## Empty User-defined Class
```
0 Person	48
1 Person	7352
10 Person	7496
20 Person	7656
30 Person	7816
```
从1->10, 10->20, 20->30这几个增量，可以看出每个Empty User-defined Class占用16个字节，包括12字节的Object Header以及4字节的对象间内存对齐


## User-defined Class with One int Field
```
0 Person	48
1 Person	7416
10 Person	7560
20 Person	7720
30 Person	7880
```
从1->10, 10->20, 20->30这几个增量，可以看出每个User-defined Class with One int Field占用16个字节，包括12字节的Object Header以及4字节的int


## 测试环境2
```
openjdk version "11.0.4" 2019-07-16
OpenJDK Runtime Environment (build 11.0.4+11-post-Ubuntu-1ubuntu218.04.3)
OpenJDK 64-Bit Server VM (build 11.0.4+11-post-Ubuntu-1ubuntu218.04.3, mixed mode, sharing)

```

## Long
```
0 Long	48
1 Long	344
10 Long	560
20 Long	800
30 Long 1040
```

## Empty User-defined Class
```
0 Person	48
1 Person	3800
10 Person	3944
20 Person	4104
30 Person	4264
```

# 测试环境3
```
openjdk version "1.7.0_242"
OpenJDK Runtime Environment (Zulu 7.34.0.5-CA-linux64) (build 1.7.0_242-b7)
OpenJDK 64-Bit Server VM (Zulu 7.34.0.5-CA-linux64) (build 24.242-b7, mixed mode)
```

## Long
```
0 Long	48
1 Long	456
10 Long	672
20 Long	912
30 Long 1152
```

## Empty User-defined Class
```
0 Person	48
1 Person	8440
10 Person	8584
20 Person	8744
30 Person	8904
```