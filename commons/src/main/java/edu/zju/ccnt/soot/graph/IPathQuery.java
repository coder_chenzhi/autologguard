package edu.zju.ccnt.soot.graph;


public interface IPathQuery {
    public boolean hasPath(Object src, Object dest);
}